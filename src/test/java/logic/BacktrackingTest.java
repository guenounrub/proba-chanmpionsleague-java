package logic;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.LinkedList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import teams.logic.Backtracking;
import teams.model.Chapeaux;
import teams.model.Country;
import teams.model.Team;
import teams.model.Tirage;

public class BacktrackingTest {
	Backtracking backtracking;
	
	Team bayernMunich;
	Team realMadrid;
	Team manCity;
	Team liverpool;
	Team chelsea;
	Team dortmund;
	Team juventus;
	Team psg;


	Team lazioRome;
	LinkedList<Team> blackListOfLazioRome, opponentsOfLazioRome;
	@BeforeEach
	public void init(){
		bayernMunich = new Team("Bayern Munich",'A',1,Country.DEUTSCHLAND);
		realMadrid = new Team("Real Madrid",'B',1,Country.SPAIN);
		manCity = new Team("Man.City",'C',1,Country.ENGLAND);
		liverpool = new Team("Liverpool",'D',1,Country.ENGLAND);
		chelsea = new Team("Chelsea",'E',1,Country.ENGLAND);
		dortmund = new Team("Dortmund", 'F', 1, Country.DEUTSCHLAND);
		juventus = new Team("Juventus",'G',1,Country.ITALY);
		psg = new Team("PSG",'H',1,Country.FRANCE);
		
		lazioRome = new Team("Lazio Rome",'F',2,Country.ITALY);
		
		Chapeaux chapeaux = new Chapeaux();
		blackListOfLazioRome = new LinkedList<Team>();
		blackListOfLazioRome.add(manCity);
		blackListOfLazioRome.add(realMadrid);

		opponentsOfLazioRome = new LinkedList<Team>();
		opponentsOfLazioRome.add(bayernMunich);
		opponentsOfLazioRome.add(realMadrid);
		opponentsOfLazioRome.add(manCity);
		opponentsOfLazioRome.add(liverpool);
		opponentsOfLazioRome.add(chelsea);
		opponentsOfLazioRome.add(psg);
		Tirage tirage = new Tirage();
		backtracking = new Backtracking(chapeaux, tirage);
	}

	@Test
	public void givenParametersIsMatchPossibletest(){
		assertAll(
			() -> assertTrue(backtracking.isChallengerAPossibleOpponentOfCandidate(bayernMunich, lazioRome, blackListOfLazioRome)),
			() -> assertFalse(backtracking.isChallengerAPossibleOpponentOfCandidate(bayernMunich, realMadrid, blackListOfLazioRome))
		);
	}

	@Test
	public void givenACandidateCreateListOfOpponentsWithoutBlacklistTest(){
		LinkedList<Team> opponents = backtracking.createListOfOpponentsOfCandidateFromChapeauOfFirst(lazioRome, new LinkedList<Team>());
		assertEquals(opponentsOfLazioRome, opponents);
	}

	@Test
	public void givenACandidateCreateListOfOpponentsWithBlacklistTest(){
		LinkedList<Team> opponents = backtracking.createListOfOpponentsOfCandidateFromChapeauOfFirst(lazioRome, blackListOfLazioRome);
		opponentsOfLazioRome.remove(manCity);
		opponentsOfLazioRome.remove(realMadrid);
		assertEquals(opponentsOfLazioRome, opponents);
	}

	@Test
	public void randomSelectTest(){
		Team opponent = backtracking.selectARandomOpponent(opponentsOfLazioRome);
		assertTrue(opponentsOfLazioRome.contains(opponent));
	}


}
