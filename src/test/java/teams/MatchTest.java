package teams;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import teams.model.Country;
import teams.model.Match;
import teams.model.Team;

public class MatchTest {
	
	Team emptyTeam, juventus, roma, chelsea, donetsk;

	@BeforeEach
	protected void init(){
		emptyTeam = new Team();
		juventus = new Team("Juventus", 'D', 2, Country.ITALY);
		roma = new Team("AS Roma", 'C', 1, Country.ITALY);
		chelsea = new Team("Chelsea", 'C', 2, Country.ENGLAND);
		donetsk = new Team("Chaktior Donetsk",'F',2,Country.UKRAINE);
	}

	@Test
	public void isMatchPossibleTest(){
		assertAll(
			() -> assertFalse(Match.isMatchPossible(juventus, emptyTeam)),
			() -> assertFalse(Match.isMatchPossible(emptyTeam, juventus)),
			() -> assertFalse(Match.isMatchPossible(juventus, roma)),
			() -> assertTrue(Match.isMatchPossible(roma, donetsk))
		);
	}

}
