package teams;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import teams.model.Chapeaux;
import teams.model.Tirage;

public class TirageTest {
	Tirage tirage;
	Chapeaux chapeaux;

	@BeforeEach
	private void init(){
		tirage = new Tirage();
		chapeaux = new Chapeaux();
	}

	@Test
	public void selectTeamFromChapeauTest(){
		tirage.selectTeamsFromChapeauOfSecond(chapeaux);
		assertTrue(chapeaux.isChapeauOfSecondEmpty());
	}
}
