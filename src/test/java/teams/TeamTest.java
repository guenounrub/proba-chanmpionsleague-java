package teams;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import teams.model.Country;
import teams.model.Team;

public class TeamTest {
	
	Team emptyTeam, juventus, roma, chelsea;

	@BeforeEach
	protected void init(){
		emptyTeam = new Team();
		juventus = new Team("Juventus", 'D', 2, Country.ITALY);
		roma = new Team("AS Roma", 'C', 1, Country.ITALY);
		chelsea = new Team("Chelsea", 'C', 2, Country.ENGLAND);
	}

	@Test
	public void givenTeamsIsEmptyTest(){
		assertAll("isEmpty",
			() -> assertTrue(emptyTeam.isEmptyTeam()),
			() -> assertFalse(juventus.isEmptyTeam()),
			() -> assertFalse(roma.isEmptyTeam())
		);
	}

	@Test
	public void givenTeamsEqualsTest(){
		assertAll("equals",
			() -> assertFalse(emptyTeam.equals(juventus)),
			() -> assertTrue(juventus.equals(juventus)),
			() -> assertFalse(roma.equals(juventus))
		);
	}

}
