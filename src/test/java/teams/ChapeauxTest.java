package teams;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import teams.model.Chapeaux;
import teams.model.Country;
import teams.model.Team;

public class ChapeauxTest {
	Chapeaux chapeaux;
	Team[] emptyChapeauOfSeconds, chapeauOfSeconds;

	@BeforeEach
	private void init(){
		chapeaux = new Chapeaux();
		emptyChapeauOfSeconds = new Team[Chapeaux.numberOfTeamsPerChapeau];
		for (int i = 0; i < Chapeaux.numberOfTeamsPerChapeau; i++) {
			emptyChapeauOfSeconds[i] = new Team();
		}

		chapeauOfSeconds = chapeaux.getChapeauOfSecondsOfGroup();
	}

	@Test
	public void selectTeamFromChapeauTest(){
		System.out.println(chapeaux);
		Team testTeam = chapeaux.selectTeamFromChapeauOfSeconds();
		System.out.println(testTeam);
		System.out.println(chapeaux);
	}

	@Test
	public void givenAnEmptyChapeauOfSecondsIsChapeauOfSecondEmptyTest(){
		chapeaux.setChapeauOfSecondsOfGroup(emptyChapeauOfSeconds);
		assertTrue(chapeaux.isChapeauOfSecondEmpty());
	}

	@Test
	public void givenANotEmptyChapeauOfSecondsIsChapeauOfSecondEmptyTest(){
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		System.out.println(chapeaux);
		assertFalse(chapeaux.isChapeauOfSecondEmpty());
	}

	@Test
	public void givenANotEmptyChapeauOfSecondsIsChapeauOfSecondEmptyAnotherTest(){
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		System.out.println(chapeaux);
		assertTrue(chapeaux.isChapeauOfSecondEmpty());
	}

	@Test
	public void givenAFullChapeauOfSecondsIsChapeauOfSecondFullTest(){
		assertTrue(chapeaux.isChapeauOfSecondFull());
	}

	@Test
	public void givenANotEmptyChapeauOfSecondsIsChapeauOfSecondFullTest(){
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		chapeaux.selectTeamFromChapeauOfSeconds();
		System.out.println(chapeaux);
		assertFalse(chapeaux.isChapeauOfSecondFull());
	}

	@Test
	public void givenAnEmptyChapeauOfSecondsIsChapeauOfSecondFullTest(){
		chapeaux.setChapeauOfSecondsOfGroup(emptyChapeauOfSeconds);
		assertFalse(chapeaux.isChapeauOfSecondFull());
	}

	@Test
	public void givenMunichTeamIsInTheFirstGroupShouldBeTrue(){
		Team bayernMunich = new Team("Bayern Munich",'A',1,Country.DEUTSCHLAND);
		assertTrue(chapeaux.isInTheFirstGroup(bayernMunich));
	}

	@Test
	public void givenAtleticoMadridTeamIsInTheFirstGroupShouldBeFalse(){
		Team atlMadrid = new Team("Atletico Madrid", 'A', 2, Country.SPAIN);
		assertFalse(chapeaux.isInTheFirstGroup(atlMadrid));
	}

	@Test
	public void givenMunichTeamIsInTheSecondGroupShouldBeTrue(){
		Team bayernMunich = new Team("Bayern Munich",'A',1,Country.DEUTSCHLAND);
		assertFalse(chapeaux.isInTheSecondGroup(bayernMunich));
	}

	@Test
	public void givenAtleticoMadridTeamIsInTheSecondGroupShouldBeTrue(){
		Team atlMadrid = new Team("Atletico Madrid", 'A', 2, Country.SPAIN);
		assertTrue(chapeaux.isInTheSecondGroup(atlMadrid));
	}

	@Test
	public void givenMunichTeamGetIndexOfTeamFromFirstShouldBeZero(){
		Team bayernMunich = new Team("Bayern Munich",'A',1,Country.DEUTSCHLAND);
		assertEquals(0, chapeaux.getIndexOfTeamFromFirstGroup(bayernMunich));
	}

	@Test 
	public void givenATeamNotInTheFirstGroupGetIndexOfTeamShouldThrownException(){
		Team atlMadrid = new Team("Atletico Madrid", 'A', 2, Country.SPAIN);
		assertThrows(IllegalArgumentException.class, () -> {chapeaux.getIndexOfTeamFromFirstGroup(atlMadrid);});
	}

	@Test
	public void givenMunichTeamGetIndexOfTeamFromSecondShouldThrownException(){
		Team bayernMunich = new Team("Bayern Munich",'A',1,Country.DEUTSCHLAND);
		assertThrows(IllegalArgumentException.class, () -> {chapeaux.getIndexOfTeamFromSecondGroup(bayernMunich);});
	}
	
	@Test 
	public void givenAtlMadridTeamGetIndexOfTeamFromSecondShouldReturnZero(){
		Team atlMadrid = new Team("Atletico Madrid", 'A', 2, Country.SPAIN);
		assertEquals(0, chapeaux.getIndexOfTeamFromSecondGroup(atlMadrid));
	}
}

