package teams.model;

public class Tirage {
	private Match[] matches;

	public Tirage(){
		this.matches = new Match[Chapeaux.numberOfTeamsPerChapeau];
		for (int i = 0; i < matches.length; i++) {
			this.matches[i] = new Match();
		}
	}

	public void setMatch(int index, Match match){
		this.matches[index] = match;
	}

	public void setMatch(int index, Team firstTeam, Team secondTeam){
		this.matches[index] = new Match(firstTeam, secondTeam);
	}

	public Match getMatch(int index){
		return matches[index];
	}

	public Match[] getMatches(){
		return this.matches;
	}

	public void selectTeamsFromChapeauOfSecond(Chapeaux chapeaux){
		for (int i = 0; i < Chapeaux.numberOfTeamsPerChapeau; i++) {
			if(!chapeaux.isChapeauOfSecondEmpty()){
				matches[i].setSecondTeam(chapeaux.selectTeamFromChapeauOfSeconds());
			}

		}
	}


	@Override
	public String toString() {
		String str = "";
		for (int i = 0; i < matches.length; i++) {
			str += matches[i].toString() + "\n";
		}
		return str;
	}
}
