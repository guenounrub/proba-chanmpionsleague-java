package teams.model;

@SuppressWarnings("unused")
public class Chapeaux {
	public static Integer numberOfTeamsPerChapeau = 8;

	//Tirage 2020-2021
	
	private static Team bayernMunich = new Team("Bayern Munich",'A',1,Country.DEUTSCHLAND);
	private static Team realMadrid = new Team("Real Madrid",'B',1,Country.SPAIN);
	private static Team manCity = new Team("Man.City",'C',1,Country.ENGLAND);
	private static Team liverpool = new Team("Liverpool",'D',1,Country.ENGLAND);
	private static Team chelsea = new Team("Chelsea",'E',1,Country.ENGLAND);
	private static Team dortmund = new Team("Dortmund", 'F', 1, Country.DEUTSCHLAND);
	private static Team juventus = new Team("Juventus",'G',1,Country.ITALY);
	private static Team psg = new Team("PSG",'H',1,Country.FRANCE);

	private static Team atlMadrid = new Team("Atletico Madrid", 'A', 2, Country.SPAIN);
	private static Team mGladbach = new Team("Mönchengladbach",'B',2,Country.DEUTSCHLAND);
	private static Team porto = new Team("FC Porto",'C',2,Country.PORTUGAL);
	private static Team bergame = new Team("Atalanta Bergame", 'D', 2, Country.ITALY);
	private static Team seville = new Team("FC Seville",'E',2,Country.SPAIN);
	private static Team lazioRome = new Team("Lazio Rome",'F',2,Country.ITALY);
	private static Team fcBarcelone = new Team("FC Barcelone",'G',2,Country.SPAIN);
	private static Team leipzig = new Team("RB Leipzig", 'H', 2, Country.DEUTSCHLAND);
	

	//Autres Equipes possible en ldc
	private static Team asRome = new Team("AS Rome",'C',1,Country.ITALY);
	private static Team besiktas = new Team("Besiktas",'G',1,Country.TURKEY);
	private static Team manUnited = new Team("Man.United",'A',1,Country.ENGLAND);
	private static Team tottenham = new Team("Tottenham",'H',1,Country.ENGLAND);
	private static Team bale = new Team("FC Bale",'A',2,Country.SWITZERLAND);
	private static Team donetsk = new Team("Chaktior Donetsk",'F',2,Country.UKRAINE);


	
	private Team[] chapeauOfFirstsOfGroup;
	private Team[] chapeauOfSecondsOfGroup;

	public Chapeaux(){
		chapeauOfFirstsOfGroup = new Team[numberOfTeamsPerChapeau];
		chapeauOfFirstsOfGroup[0] = bayernMunich;
		chapeauOfFirstsOfGroup[1] = realMadrid;
		chapeauOfFirstsOfGroup[2] = manCity;
		chapeauOfFirstsOfGroup[3] = liverpool;
		chapeauOfFirstsOfGroup[4] = chelsea;
		chapeauOfFirstsOfGroup[5] = dortmund;
		chapeauOfFirstsOfGroup[6] = juventus;
		chapeauOfFirstsOfGroup[7] = psg;

		chapeauOfSecondsOfGroup = new Team[numberOfTeamsPerChapeau];
		chapeauOfSecondsOfGroup[0] = atlMadrid;
		chapeauOfSecondsOfGroup[1] = mGladbach;
		chapeauOfSecondsOfGroup[2] = porto;
		chapeauOfSecondsOfGroup[3] = bergame;
		chapeauOfSecondsOfGroup[4] = seville;
		chapeauOfSecondsOfGroup[5] = lazioRome;
		chapeauOfSecondsOfGroup[6] = fcBarcelone;
		chapeauOfSecondsOfGroup[7] = leipzig;
	}


	public Team[] getChapeauOfFirstsOfGroup() {
		return this.chapeauOfFirstsOfGroup;
	}

	public Team[] getChapeauOfSecondsOfGroup() {
		return this.chapeauOfSecondsOfGroup;
	}

	public void setChapeauOfSecondsOfGroup(Team[] newChapeau) {
		this.chapeauOfSecondsOfGroup = newChapeau;
	}
	

	public Team selectTeamFromChapeauOfSeconds(){
		Team selectedTeam;
		int i;
		do{
			i = (int) (Math.random() * (Chapeaux.numberOfTeamsPerChapeau));
		}while(!isChapeauOfSecondEmpty() && this.chapeauOfSecondsOfGroup[i].isEmptyTeam());
		selectedTeam = this.chapeauOfSecondsOfGroup[i];
		this.chapeauOfSecondsOfGroup[i] = new Team();
		return selectedTeam;
	}

	public boolean isChapeauOfSecondEmpty(){
		boolean isEmpty = true;
		for (int i = 0; i < chapeauOfSecondsOfGroup.length; i++) {
			if(!chapeauOfSecondsOfGroup[i].isEmptyTeam()) isEmpty = false;
		}
		return isEmpty;
	}

	public void addTeamToChapeauOfSeconds(Team teamToAdd){
		if(!isChapeauOfSecondFull()){
			int i = 0;
			while(i <= numberOfTeamsPerChapeau && !chapeauOfSecondsOfGroup[i].isEmptyTeam()){
				i++;
			}
			chapeauOfSecondsOfGroup[i] = teamToAdd;
		}
	}

	public boolean isChapeauOfSecondFull() {
		boolean isFull = true;
		for (int i = 0; i < chapeauOfSecondsOfGroup.length; i++) {
			if(chapeauOfSecondsOfGroup[i].isEmptyTeam()) isFull = false;
		}
		return isFull;
	}

	public boolean isChapeauOfFirstEmpty(){
		boolean isEmpty = true;
		for (int i = 0; i < chapeauOfFirstsOfGroup.length; i++) {
			if(!chapeauOfFirstsOfGroup[i].isEmptyTeam()) isEmpty = false;
		}
		return isEmpty;
	}

	public void addTeamToChapeauOfFirst(Team teamToAdd){
		if(!isChapeauOfSecondFull()){
			int i = 0;
			while(i <= numberOfTeamsPerChapeau && !chapeauOfFirstsOfGroup[i].isEmptyTeam()){
				i++;
			}
			chapeauOfFirstsOfGroup[i] = teamToAdd;
		}
	}

	public boolean isChapeauOfFirstFull() {
		boolean isFull = true;
		for (int i = 0; i < chapeauOfFirstsOfGroup.length; i++) {
			if(chapeauOfFirstsOfGroup[i].isEmptyTeam()) isFull = false;
		}
		return isFull;
	}

	public Team getTeamFromChapeauOfSeconds(int index){
		if(index < 0 && index > numberOfTeamsPerChapeau) 
			throw new IndexOutOfBoundsException("Invalid index : "+index);
		return chapeauOfSecondsOfGroup[index];
	}

	public Team getTeamFromChapeauOfFirsts(int index){
		if(index < 0 && index > numberOfTeamsPerChapeau) 
			throw new IndexOutOfBoundsException("Invalid index : "+index);
		return chapeauOfFirstsOfGroup[index];
	}

	public void removeFromChapeauOfFirsts(Team team){
		for (int i = 0; i < chapeauOfFirstsOfGroup.length; i++) {
			if (chapeauOfFirstsOfGroup[i].equals(team)) chapeauOfFirstsOfGroup[i] = new Team();
		}
	}

	public int getIndexOfTeamFromFirstGroup(Team team){
		if(notInTheFirstGroup(team)) throw new IllegalArgumentException("The team"+team+" is not in the chapeau of firsts");
		int index = -1;
		for (int i = 0; i < chapeauOfFirstsOfGroup.length; i++) {
			if (chapeauOfFirstsOfGroup[i].equals(team)) index = i;
		}
		return index;
	}
	

	private boolean notInTheFirstGroup(Team team) {
		return !isInTheFirstGroup(team);
	}

	public boolean isInTheFirstGroup(Team team) {
		boolean isInFirstGroup = false;
		for (int i = 0; i < chapeauOfFirstsOfGroup.length; i++) {
			if (chapeauOfFirstsOfGroup[i].equals(team)) isInFirstGroup = true;
		}
		return isInFirstGroup;
	}


	public int getIndexOfTeamFromSecondGroup(Team team){
		if(notInTheSecondGroup(team)) throw new IllegalArgumentException("The team"+team+" is not in the chapeau of seconds");
		int index = -1;
		for (int i = 0; i < chapeauOfSecondsOfGroup.length; i++) {
			if (chapeauOfSecondsOfGroup[i].equals(team)) index = i;
		}
		return index;
	}
	

	private boolean notInTheSecondGroup(Team team) {
		return !isInTheSecondGroup(team);
	}

	public boolean isInTheSecondGroup(Team team) {
		boolean isInSecondGroup = false;
		for (int i = 0; i < chapeauOfSecondsOfGroup.length; i++) {
			if (chapeauOfSecondsOfGroup[i].equals(team)) isInSecondGroup = true;
		}
		return isInSecondGroup;
	}

	@Override
	public String toString() {
		return "{" +
			" chapeauOfFirstsOfGroup=' \n" + chapeauToString(chapeauOfFirstsOfGroup) + "'" +
			", \nchapeauOfSecondsOfGroup=' \n" + chapeauToString(chapeauOfSecondsOfGroup) + "'" +
			"}";
	}

	private static String chapeauToString(Team[] chapeau){
		String str = "[";
		for (int i = 0; i < chapeau.length-1; i++) {
			str += chapeau[i].toString() + ",";
		}
		str += chapeau[7].toString() + "]";
		return str;
	}



}
