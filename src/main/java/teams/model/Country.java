package teams.model;

public enum Country {
	ITALY("Italy"),
	ENGLAND("England"),
	FRANCE("France"),
	TURKEY("Turkey"),
	SWITZERLAND("Switzerland"),
	DEUTSCHLAND("Deutschland"),
	SPAIN("Spain"),
	UKRAINE("Ukrain"),
	PORTUGAL("Portugal"),
	EMPTY("");

	String country;
	private Country(String country) {
		this.country = country;
	}

	
}
