package teams.model;

public class Match {
	//fistTeam est toujours l'équipe du chapeau 1 et secondTeam celle du chapeau 2
	private Team firstTeam;
	private Team secondTeam;

	public Match(){
		this.firstTeam = new Team();
		this.secondTeam = new Team();
	}

	public Match(Team firstTeam, Team secondTeam) {
		this.firstTeam = firstTeam;
		this.secondTeam = secondTeam;
	}

	public Team getFirstTeam() {
		return this.firstTeam;
	}

	public Team getSecondTeam() {
		return this.secondTeam;
	}

	public void setFirstTeam(Team firstTeam) {
		this.firstTeam = firstTeam;
	}
	public void setSecondTeam(Team secondTeam) {
		this.secondTeam = secondTeam;
	}


	@Override
	public String toString() {
		return firstTeam + " vs " + secondTeam;
	}

	public static boolean isMatchPossible(Team firstTeam, Team secondTeam){
		if(firstTeam.isEmptyTeam() || 
		   secondTeam.isEmptyTeam() || firstTeam.equals(secondTeam)) return false;
		else{
			boolean isPossible = (!firstTeam.getName().equals(secondTeam.getName())) &&
								 firstTeam.getGroup() != secondTeam.getGroup() &&
								 (!firstTeam.getStanding().equals(secondTeam.getStanding()) &&
								 (!firstTeam.getCountry().equals(secondTeam.getCountry())));  
			return isPossible;
		}
	}

}
