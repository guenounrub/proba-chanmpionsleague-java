package teams.model;

import java.util.Objects;

public class Team {
	private String name;
	private char group;
	private Integer standing;
	private Country country;


	public Team() {
		this.name = "Empty";
		this.group = 'E';
		this.standing = 0;
		this.country = Country.EMPTY;
	}

	public Team(String name, char group, Integer standing, Country country) {
		this.name = name;
		this.group = group;
		this.standing = standing;
		this.country = country;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getGroup() {
		return this.group;
	}

	public void setGroup(char group) {
		this.group = group;
	}

	public Integer getStanding() {
		return this.standing;
	}

	public void setStanding(Integer standing) {
		this.standing = standing;
	}

	public String getCountry() {
		return this.country.name();
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof Team)) {
			return false;
		}
		Team equipe = (Team) o;
		return Objects.equals(name, equipe.name) 
				&& Objects.equals(group, equipe.group) 
				&& Objects.equals(standing, equipe.standing) 
				&& Objects.equals(country, equipe.country);
	}

	@Override
	public String toString() {
		return getName();
	}

	public boolean isEmptyTeam(){
		Team emptyTeam = new Team();
		return this.equals(emptyTeam);
	}


}
