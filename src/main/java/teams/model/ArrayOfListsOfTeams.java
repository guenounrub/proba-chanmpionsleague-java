package teams.model;

import java.util.LinkedList;

@SuppressWarnings("unchecked")
public class ArrayOfListsOfTeams {
	private static final int numberOfLists = 8;
	private LinkedList<Team>[] array;


	public ArrayOfListsOfTeams() {
		array = new LinkedList[numberOfLists];
		for (int i = 0; i < numberOfLists; i++) {
			this.array[i] = new LinkedList<Team>();
		}
	}


	public ArrayOfListsOfTeams(LinkedList<Team>[] array) {
		this.array = array;
	}

	public LinkedList<Team>[] getArray() {
		return this.array;
	}

	public void setArray(LinkedList<Team>[] array) {
		this.array = array;
	}

	public void setList(int index, LinkedList<Team> list){
		this.array[index] = list;
	}

	public LinkedList<Team> getList(int index){
		if(index < 0 || index >= numberOfLists) throw new IndexOutOfBoundsException();
		else return array[index];
	}

	public void clearList(int index){
		array[index].clear();
	}

	@Override
	public String toString() {
		String str = "[";
		for (int i = 0; i < numberOfLists; i++) {
			str +="{";
			for (LinkedList<Team> linkedList : array) {
				for (Team team : linkedList) {
					str += team.toString()+',';
				}
			}
			str +="}";
		}
		str += "]";
		return str;
	}

}
