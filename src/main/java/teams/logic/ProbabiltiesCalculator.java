package teams.logic;

import teams.model.Chapeaux;
import teams.model.Match;
import teams.model.Team;
import teams.model.Tirage;

public class ProbabiltiesCalculator {
	private int globalnumberOfMatch;
	private int[][] numbersOfMatches;
	private static int nbOfTeamsPerChapeau = Chapeaux.numberOfTeamsPerChapeau;

	public ProbabiltiesCalculator(ProbabilitiesSimulator simulator) {
		globalnumberOfMatch = simulator.getNumberOfSimulations() * 8;
		numbersOfMatches = new int[nbOfTeamsPerChapeau][nbOfTeamsPerChapeau];
		proccessResultsFromSimulator(simulator);
	}

	private void proccessResultsFromSimulator(ProbabilitiesSimulator simulator) {
		Tirage[] allTirages = simulator.getResultsOfSimulations();
		for(Tirage tirage : allTirages){
			upgradeCalculator(tirage);
		}
	}

	private void upgradeCalculator(Tirage tirage) {
		Match[] matchesFromTirage = tirage.getMatches();
		for(Match match: matchesFromTirage){
			upgradeTable(match);
		}
	}

	private void upgradeTable(Match match) {
		Chapeaux chapeaux = new Chapeaux();
		int i = chapeaux.getIndexOfTeamFromFirstGroup(match.getFirstTeam());
		int j = chapeaux.getIndexOfTeamFromSecondGroup(match.getSecondTeam());
		this.numbersOfMatches[i][j]++;
	}

	public int getTotalOfComputedMatches(){
		int total = 0;
		for (int i = 0; i < nbOfTeamsPerChapeau; i++) {
			for (int j = 0; j < nbOfTeamsPerChapeau; j++) {
				total += numbersOfMatches[i][j];
			}
		}
		return total;
	}

	@Override
	public String toString() {
		return "{" +
			" globalnumberOfMatch='" + globalnumberOfMatch + 
			// "'  total of computed match = " + getTotalOfComputedMatches() +
			", numbersOfMatches='" + displayTable() + "'" +
			", probabilities = " + displayProba() +
			"}";
	}

	private String displayTable(){
		String str = "";
		Chapeaux chapeaux = new Chapeaux();
		for (int i = 0; i < nbOfTeamsPerChapeau; i++) {
			Team firstTeam = chapeaux.getTeamFromChapeauOfFirsts(i);
			for (int j = 0; j < nbOfTeamsPerChapeau; j++) {
				Team secondTeam = chapeaux.getTeamFromChapeauOfSeconds(j);
				str += firstTeam +" vs "+ secondTeam+ " = "+ numbersOfMatches[i][j]+"\n";
			}
		}
		return str;
	}

	private String displayProba(){
		String str = "\n";
		Chapeaux chapeaux = new Chapeaux();
		for (int i = 0; i < nbOfTeamsPerChapeau; i++) {
			Team firstTeam = chapeaux.getTeamFromChapeauOfFirsts(i);
			for (int j = 0; j < nbOfTeamsPerChapeau; j++) {
				Team secondTeam = chapeaux.getTeamFromChapeauOfSeconds(j);
				Double proba = Double.valueOf((Double.valueOf(numbersOfMatches[i][j]) / Double.valueOf(globalnumberOfMatch)));
				proba *= 1000;
				// System.out.println(proba+"\n");
				str += String.format("%20s",firstTeam) +"    vs "+ String.format("%20s",secondTeam)+ " = "+ String.format("%10f",proba) +" %\n";
			}
		}
		return str;
	}
}