package teams.logic;

import teams.model.Chapeaux;
import teams.model.Tirage;

public class SimulationOfTirage {
	private static final int numberOfTirage = 8;
	Backtracking backtracking;
	
	public SimulationOfTirage(){
		Chapeaux chapeaux = new Chapeaux();
		Tirage tirage = new Tirage();
		this.backtracking = new Backtracking(chapeaux, tirage);
	}

	public Tirage runTirage(){
		int j = 0;
		while( j >= 0 && j < numberOfTirage){
			j = backtracking.selectAMatch(j) + 1;
		}
		return backtracking.getTirage();
	}

	// public void writeTirage
}
