package teams.logic;

import teams.model.Tirage;

public class ProbabilitiesSimulator {
	private int numberOfSimulations;
	private Tirage[] simulator;

	public ProbabilitiesSimulator(int numberOfSimulations){
		this.numberOfSimulations = numberOfSimulations;
		simulator = new Tirage[numberOfSimulations];
		for (int i = 0; i < numberOfSimulations; i++) {
			SimulationOfTirage simulation = new SimulationOfTirage();
			simulator[i] = simulation.runTirage();
		}
		// ProbabiltiesCalculator calculator = new ProbabiltiesCalculator(this);
		// System.out.println(calculator);
		displayResults();
	}

	private void displayResults() {
		for (int i = 0; i < numberOfSimulations; i++) {
			// System.out.println("Tirage "+ i + "\n");
			System.out.print(simulator[i]);
		}
	}

	public int getNumberOfSimulations(){
		return this.numberOfSimulations;
	}

	public Tirage[] getResultsOfSimulations(){
		return this.simulator;
	}
}
