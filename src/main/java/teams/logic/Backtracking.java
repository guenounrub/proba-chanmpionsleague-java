package teams.logic;

import java.util.LinkedList;

import teams.model.ArrayOfListsOfTeams;
import teams.model.Chapeaux;
import teams.model.Match;
import teams.model.Team;
import teams.model.Tirage;

public class Backtracking {
	Chapeaux chapeaux;
	ArrayOfListsOfTeams listOfOpponents;
	ArrayOfListsOfTeams blacklist;
	Tirage tirage;
	boolean alreadyVisited;
	

	public Backtracking(Chapeaux chapeaux, Tirage tirage) {
		this.chapeaux = chapeaux;
		this.listOfOpponents = new ArrayOfListsOfTeams();
		this.blacklist = new ArrayOfListsOfTeams();
		this.tirage = tirage;
		tirage.selectTeamsFromChapeauOfSecond(chapeaux);
		this.alreadyVisited = false;
	}


	@Override
	public String toString() {
		return "{" +
			" chapeaux='" + chapeaux + "'" +
			", listOfOpponents='" + listOfOpponents + "'" +
			", blacklist='" + blacklist + "'" +
			", tirage='" + tirage + "'" +
			", alreadyVisited='" + alreadyVisited + "'" +
			"}";
	}


	public int selectAMatch(int numerousOfMatch){
		// System.out.println("numero = "+numerousOfMatch);
		// System.out.println(tirage);
		Team candidateFromChapeauOfSeconds = tirage.getMatch(numerousOfMatch).getSecondTeam();
		if(alreadyVisited) listOfOpponents.clearList(numerousOfMatch);
		createListOfOpponents(numerousOfMatch, candidateFromChapeauOfSeconds);
		if(listOfOpponents.getList(numerousOfMatch).isEmpty()){
			applyBacktracking(numerousOfMatch);
			alreadyVisited = true;
			return numerousOfMatch - 2;
		}else{
			LinkedList<Team> opponentsFromchapeauOfFirst = listOfOpponents.getList(numerousOfMatch);
			Team opponent = selectARandomOpponent(opponentsFromchapeauOfFirst);
			tirage.setMatch(numerousOfMatch, opponent, candidateFromChapeauOfSeconds);
			alreadyVisited = false;
			return numerousOfMatch;
		}
	}

	private void createListOfOpponents(int numerousOfMatch, Team candidateFromChapeauOfSeconds) {
		LinkedList<Team> opponents = createListOfOpponentsOfCandidateFromChapeauOfFirst(candidateFromChapeauOfSeconds, blacklist.getList(numerousOfMatch));
		listOfOpponents.setList(numerousOfMatch, opponents);
	}

	public LinkedList<Team> createListOfOpponentsOfCandidateFromChapeauOfFirst(Team candidate, LinkedList<Team> blacklistOfCandidate){
		LinkedList<Team> listOfOpponents = new LinkedList<Team>();
		Team[] chapeauOfFirst = chapeaux.getChapeauOfFirstsOfGroup();
		for (int i = 0; i < Chapeaux.numberOfTeamsPerChapeau; i++) {
			Team challenger = chapeauOfFirst[i];
			if(isChallengerAPossibleOpponentOfCandidate(candidate, challenger, blacklistOfCandidate)){
				listOfOpponents.add(challenger);
			}
		}
		return listOfOpponents;
	}

	public boolean isChallengerAPossibleOpponentOfCandidate(Team candidate, 
					Team challenger, LinkedList<Team> blacklistOfCandidate){
		if(challenger.isEmptyTeam()) return false;
		else if(!blacklistOfCandidate.contains(challenger) && 
				Match.isMatchPossible(candidate, challenger)) return true;
		else return false;
	}

	public Team selectARandomOpponent(LinkedList<Team> opponents){
		int numberOfOpponents = opponents.size();
		int randomIndex = (int) (Math.random() * (numberOfOpponents));
		Team opponent = opponents.get(randomIndex);
		chapeaux.removeFromChapeauOfFirsts(opponent);
		return opponent;
	}

	public void applyBacktracking(int numerousOfMatch) {
		// System.out.println("in BT");
		Match problematicMatch = tirage.getMatch(numerousOfMatch-1); // bug possible ici
		Team teamToBlacklist = problematicMatch.getFirstTeam();
		blacklist.getList(numerousOfMatch-1).add(teamToBlacklist); // bug possible ici
		resetAllOthersBlacklist(numerousOfMatch);
		chapeaux.addTeamToChapeauOfFirst(teamToBlacklist);
	}

	private void resetAllOthersBlacklist(int numerousOfMatch) {
		for (int i = numerousOfMatch; i < blacklist.getArray().length; i++) {
			blacklist.clearList(i);
		}
	}

	public Tirage getTirage(){
		return this.tirage;
	}


}
